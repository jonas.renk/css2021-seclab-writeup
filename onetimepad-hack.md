# OneTimePad Hack

Gegeben sind mit XOR-Chyper verschlüsselten Nachrichten,
die Teilweise entschlüsselt wurden.
Wir sollen den Schlüssel extrahieren
und die Nachrichten Komplet entschlüsseln.

## Lösung

### Extrahieren der Schlüssel

Die allgemeine Gleichung zum verschlüsseln von Nachrichten lautet
```
c = m ^ k
```
Wenn wir also eine Nachricht und deren Cyphertext kennen,
können wir daraus den Schlüssel berechnen:
```
k = c ^ m
```
Das machen wir dann mit `c1` und `c2` da wird dort Teile der Nachricht kennen
```python
In [1]: c1 = [0x3e, 0x44, 0x33, 0x26, 0x2d, 0x4d, 0x0d, 0x57, 0x64, 0x37, 0x03, 0x5f, 0x65, 0x02, 0x77, 0x1a]

In [2]: c2 = [0x7e, 0x78, 0x00, 0x28, 0x31, 0x6c, 0x20, 0x75, 0x25, 0x16, 0x04, 0x53, 0x42, 0x2f, 0x27, 0x3c]

In [3]: m = [ord(c) for c in 'tuidtuid']

In [4]: k1 = map(operator.xor, m, c1[:len(m)])

In [5]: k2 = map(operator.xor, m, c2[len(m):])

In [6]: k = list(k1) + list(k2)

In [7]: ''.join(map(chr, k))
Out[7]: 'J1ZBY8d3Qcm76ZNX'
```

### Entschlüsseln der Nachrichten

Damit können wir dann alle drei Cyphertexte entschlüsseln
```python
In [8]: c3 = [0x18, 0x07, 0x6a, 0x2a, 0x0d, 0x0f, 0x11, 0x6b, 0x01, 0x15, 0x1d, 0x79, 0x74, 0x0a, 0x0a, 0x10]

In [9]: def decrypt(ci):
   ...:     m = map(operator.xor, ci, k)
   ...:     return "".join(map(chr, m))
   ...:

In [10]: list(map(decrypt, [c1, c2, c3]))
Out[10]: ['tuidtuid5TnhSX9B', '4IZjhTDFtuidtuid', 'R60hT7uXPvpNBPDH']
```
