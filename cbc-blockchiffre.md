# CBC Blockchiffre

Gegen ist eine Nachricht, die wir mit CBC verschlüsseln sollen:
```
m0 := 1011
m1 := 0011
m2 := 1000
m3 := 0000
```

## Lösung

Wir nutzen die Formeln aus den Folien:
```
iv := 0110
c0 := permute(iv ^ m0) = permute(1101) = 1011
c1 := permute(c0 ^ m1) = permute(1000) = 0001
c2 := permute(c1 ^ m2) = permute(1001) = 1001
c3 := permute(c2 ^ m3) = permute(1001) = 1001
```
