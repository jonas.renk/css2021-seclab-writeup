# AES - Schlüsselaustausch II

Es sollen für eine Gruppe von `n` Personen
und für jede relevante Teilmenge dieser ein symmetrischer Schlüssel erstellt werden.
Wir sollen wissen, wie viele Schlüssel wir brauchen.

## Lösung

### Anzahl aller Schlüssel 

Sei `n > 1` die Anzahl der Personen.
Es gibt also die folgenden Personen:
```
X := { p1, ..., pn }
```
Für jede Teilmenge von `X` mit mindestens zwei Personen braucht es einen eigenen Schlüssel.
```
A ⊆ Power(X) --> kA
```
Es gibt `2^n` Elemente in der Potenzmenge von `X`,
von denen wir noch die zu kleine Mengen abziehen müssen.
Das sind `Ø`, sowie `{ pk }` für `1 ≤ k ≤ n`.
Die Formel für die Anzahl der Schlüssel lautet damit `2^n - 1 - n`.
Für `n = 141` erhalten wir also
```
2^141 ? 141 ? 1 = 2787593149816327892691964784081045188247410
```
Schlüssen insgesamt.


### Anzahl aller Schlüssel, die eine Person kennt

O.B.d.A. zählen wir nur die Schlüssen für `p1`.
`p1` braucht einen Schlüssel für jedes Gruppe von mindestens einer Person,
die nicht `p1` ist.
Das sind genau die folgenden Teilmengen:
```
Power(X \ { p1 }) \ { Ø }
```
Was uns `2^(n-1) - 1` Teilmengen bzw. Schlüssel gibt.
Für `n = 141` erhalten wir
```
2^(141 - 1) - 1 = 1393796574908163946345982392040522594123775
```
Schlüssel.
