# Diffie-Hellman II

Gegegen sind die Parameter für den Diffie-Hellman-Algorithmus:
```python
In [1]: a = 6
   ...: b = 4
   ...: g = 16
   ...: P = 19
```
Dabei kennt Alice die Zahlen `a`, `g` und `P`.
Bob kennt analog `b`, `g`, und `P`.

# Lösung

Alice und Bob Berechnen jeweils ihren
Zwischenexponenten und übertragen ihn.
```
A = g^a mod P
B = g^b mod P
```
Damit können beide den Schlüssel berechnen:
```
K = A^b mod P
  = (g^a)^b mod P 
  = g^(a*b) mod P 
  = g^(b*a) mod P 
  = (g^b)^a mod P 
  = B^a mod P 
```
Das gibt uns (siehe [Weak Hybrid Encryption](weak-hybrid-encryption) für Code):
```python
In [3]: A = modpow(g, a, P)

In [4]: B = modpow(g, b, P)

In [5]: K = modpow(B, a, P)

In [6]: A, B, K
Out[6]: (7, 5, 7)
```
