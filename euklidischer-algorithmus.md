# Euklidischer Algorithmus

Gegeben ist lediglich eine Diophantische Gleichung:
```
53*x + 737*y = 1
```

## Lösung

Herleitung und Code stehen in [Weak Hybrid Encryption](weak-hybrid-encryption).
Wir müssen nur noch einsetzen:
```python
In [1]: _, x, y = euclid(53, 737)

In [2]: x, y
Out[2]: (-292, 21)
```
