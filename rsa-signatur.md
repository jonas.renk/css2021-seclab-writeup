# RSA - Signatur

Geben ist eine peinliche Cringe-Nachricht und ein RSA-Schlüsselpaar.
Wir sollen die Nachricht signieren.

## Lösung

Die Gleichung zum Signieren mittels RSA lautet
```
sign(m) = h(m)^d mod n
```
Die Zahlen `h(m)`, `d` und `n` sind vorgegeben.
Für Code, siehe [Weak Hybrid Encryption](weak-hybrid-encryption).
Wir können direkt die Signatur berechnen:
```python
In [1]: h = 4294967295

In [2]: d, n = 136645298869, 205531456619

In [3]: modpow(h, d, n)
Out[3]: 142800933058
```
