# RSA - Schlüsselgenerierung

Gegeben sind Zwei Primzahlen p und q:
```python
In [1]: p, q = 115547, 278753
```
Wir sollen das zugehörige RSA-Schlüsselpaar generieren.

## Lösung

`n` und `φ(n)` können wir direkt berechnen:
```python
In [2]: n = p * q
In [3]: phi = (p-1) * (q-1)
```
Wir wählen e als den kleinsten möglichen Wert
und ermitteln d mit Euklids Algorithmus (siehe [Weak Hybrid Encryption](weak-hybrid-encryption)):
```python
In [4]: e = 3
In [5]: d = modinv(e, phi)
```
Das sind schon alle Werte:
```python
In [6]: n, phi, e, d
Out[6]: (32209072891, 32208678592, 3, 21472452395)
```
